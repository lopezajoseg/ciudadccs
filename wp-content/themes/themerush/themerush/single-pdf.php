<?php
$post_id = null;
$enable_full_width_post = get_post_custom_values('full_width_postthemeloy_checkbox');
?>
<!-- begin content -->            
<section id="contents" class="clearfix" style="height: 700px">
    <div class="row" style="padding-left:150px">
        <div class="container <?php
        if (of_get_option('post_layout') == '2c-r1-fixed') {
            
        } else {
            echo 'two-columns-sidebar';
        }
        ?><?php
        if (of_get_option('sidebar_center_width') == true) {
            echo " center_350";
        }
        ?>">
            <div class="sidebar_content">
                <h3>Encuentre aqui sus otras ediciones</h3>
                <input type="date" name="fecha" id="date">
                <input type="button" name="buscar" id="otras" value="BUSCAR">
            </div>
        </div>
        <div id="mostrar">

        </div>
    </div> 
</section>
<script>
    jQuery("#otras").click(function() {
        dato = jQuery('#date').val();
       // alert(dato);
        //jQuery('#mostrar').load('wp-content/themes/themerush/themerush/buscar-ediciones.php');
        jQuery.ajax({
            type: "POST",
            url: "wp-admin/admin-ajax.php",
            data: { action :"guias",fecha : dato },
            success: function(msg) {
                jQuery("#mostrar").append(msg);
            },
            error: function(objeto) {
                alert('Error Encontrado al ejecutar la Consulta \n' + objeto.responseText);
            }
        });
    });
</script>
