<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '123456');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Ha1lP(3y.6HV@je9ve`#@aL$N$41jS4:)-(jk}cun 2bsgPCd()_LO|2kzQpkH{G'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', 'H@}zaxsp*d-0+`xV&>TQ2-DsC^UiZ~FS2ij#SK(z+dEp,!k6P}0U!G<,9ta`>Bls'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', '%,@2H5?8-yi__b(-b(FLb>d2^xn(^h[UAM8s)HAmDz@a{Ftud)(e{*H&;r`yP$H9'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', '?Kw1p:R|I#x_rQ^Zq$;sP+F~L.!c6}ez! *%qg6&k4TsdG[q_wBN1/b$&q1d^Z8*'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', '@=1X3jQ*X6D|RqGO34F+K= ;V0l`Ue*u6hXKJd(>q+aa$URI[>w8$trN82<wjk/f'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', '6vpA+9}mH. aZ]CV,^n$hmo[%v5(1Y?_M`0h >A&n~M5jX`+6u[O+yre{SPwbbgC'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', 'vb11fc;~g%_,p*uLMO6GAh@r8x)-p)+PY?fl|QS-]Q*or+<m&QTpOhP2Oaq[Jir/'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', '_6e]t}+y%dD WC,SAH5?+&Cnf`6(B-R8e!X7?-EYJo6m2pu&]GT-<,m&<0?k(2H-'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


